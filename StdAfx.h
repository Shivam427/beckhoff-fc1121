// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H_)
#define AFX_STDAFX_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


// TODO: reference additional headers your program requires here

#include <windows.h>
#include "atc3dg.h"		// ATC3DG API
#include "sample.h"		// class declarations
#include <stdio.h>		// printf
#include <string.h>		// string handling
#include <stdlib.h>		// exit() function
#include <time.h>		// needed for time functions

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H_)
