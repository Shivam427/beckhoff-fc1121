#ifndef ESITELE_H
#define ESITELE_H

#include <iostream>
#include <windows.h>
#include <thread>
#include <string>
#include "C:\TwinCAT\AdsApi\TcAdsDll\Include\TcAdsDef.h"
#include "C:\TwinCAT\AdsApi\TcAdsDll\Include\TcAdsAPI.h"


#include <atomic>
#include <condition_variable>
#include <mutex>

struct PLCVariableNames {
    std::string leftX;
    std::string leftY;
    std::string leftZ;
    std::string leftQ0;
    std::string leftQ1;
    std::string leftQ2;
    std::string leftQ3;
    std::string rightX;
    std::string rightY;
    std::string rightZ;
    std::string rightQ0;
    std::string rightQ1;
    std::string rightQ2;
    std::string rightQ3;
    std::string H1_GRP;
    std::string H2_GRP;
    std::string lefttIsVirtualBoundary;
    std::string leftQuality;
    std::string START_BIT;
    std::string F1_FP;
    std::string F2_FP;
    std::string rightQuality;
    std::string H1_GRP_CHECK;
    std::string rightIsVirtualBoundary;
    std::string H2_GRP_CHECK;
    std::string STOP_BIT;
};

struct NDI_TELE {
    float leftX;
    float leftY;
    float leftZ;
    float leftQ0;
    float leftQ1;
    float leftQ2;
    float leftQ3;
    float rightX;
    float rightY;
    float rightZ;
    float rightQ0;
    float rightQ1;
    float rightQ2;
    float rightQ3;
    uint16_t H1_GRP;
    uint16_t H2_GRP;
    uint8_t lefttIsVirtualBoundary;
    uint8_t leftQuality;
    uint8_t START_BIT;
    uint8_t F1_FP;
    uint8_t F2_FP;
    uint8_t rightQuality;
    uint8_t H1_GRP_CHECK;
    uint8_t rightIsVirtualBoundary;
    uint8_t H2_GRP_CHECK;
    uint8_t STOP_BIT;
};

class PLCUpdater {
public:
    PLCUpdater();
    ~PLCUpdater();
    void Start();
    void Stop();

private:
    void UpdateLoop();
    void UpdateValues();

    NDI_TELE tele;
    AmsAddr m_Addr;
    long m_Handle1;
    long m_Handle2;
    long m_Handle3;
    long m_Handle4;
    long m_Handle5;
    long m_Handle6;
    long m_Handle7;
    long m_Handle8;
    long m_Handle9;
    long m_Handle10;
    long m_Handle11;
    long m_Handle12;
    long m_Handle13;
    long m_Handle14;
    long m_Handle15;
    long m_Handle16;
    long m_Handle17;
    long m_Handle18;
    long m_Handle19;
    long m_Handle20;
    long m_Handle21;
    long m_Handle22;
    long m_Handle23;
    long m_Handle24;
    long m_Handle25;
    long m_Handle26;

    std::thread m_UpdateThread;
    std::atomic<bool> m_Running;
    std::condition_variable m_Condition;
    std::mutex m_Mutex;
    PLCVariableNames m_VariableNames;
};

#endif // ESITELE_H
