#include "ConsoleApplication1.h"

#include "StdAfx.h"
#include "ATC3DG.h"


CSystem	ATC3DG;
CSensor* pSensor;
CXmtr* pXmtr;




//#pragma pack(1)

CHAR sysTimeStr[13] = {};
SYSTEMTIME systemTime;

//#define LOG_DATA


typedef unsigned short USHORT;

struct COMData
{

	int     start;
	int    SensorID;
	USHORT	quality;

	float	x;
	float	y;
	float	z;
	float	q[4];


	//int		end;
	//double	time;

};

COMData comdata1;



struct SensorData {
	int SensorID;
#ifdef ANGELS_RECORD
	DOUBLE_POSITION_ANGLES_TIME_Q_RECORD rec;
#else
	DOUBLE_POSITION_QUATERNION_TIME_Q_RECORD rec;
	//COMData comData;
#endif
	DEVICE_STATUS status;
};

SensorData sData;


#ifdef ANGELS_RECORD
DOUBLE_POSITION_ANGLES_TIME_Q_RECORD record, * pRecord = &record;
#else
DOUBLE_POSITION_QUATERNION_TIME_Q_RECORD record, * pRecord = &record;
#endif



bool errorHandler(int error);
bool InitBirdSystem(void);

bool BirdInitSuccess = false;
#ifdef ENABLE_LOGGING
struct tm* t;
time_t rawtime;
int fp;
char filename[200];
#endif



bool InitBirdSystem(void)
{
	int errorCode;

	printf("\n ATC3DG Application");

	//
	// Initialize the ATC3DG driver and DLL
	//
	// It is always necessary to first initialize the ATC3DG "system". By
	// "system" we mean the set of ATC3DG cards installed in the PC. All cards
	// will be initialized by a single call to InitializeBIRDSystem(). This
	// call will first invoke a hardware reset of each board. If at any time
	// during operation of the system an unrecoverable error occurs then the
	// first course of action should be to attempt to Recall InitializeBIRDSystem()
	// if this doesn't restore normal operating conditions there is probably a
	// permanent failure - contact tech support.
	// A call to InitializeBIRDSystem() does not return any information.
	//
	printf("\n Initializing ATC3DG system");
	errorCode = InitializeBIRDSystem();
	if (errorCode != BIRD_ERROR_SUCCESS)
		return errorHandler(errorCode);

	//
	// GET SYSTEM CONFIGURATION
	//
	// In order to get information about the system we have to make a call to
	// GetBIRDSystemConfiguration(). This call will fill a fixed size structure
	// containing amongst other things the number of boards detected and the
	// number of sensors and transmitters the system can support (Note: This
	// does not mean that all sensors and transmitters that can be supported
	// are physically attached)
	//
	printf("\n get System Config");
	errorCode = GetBIRDSystemConfiguration(&ATC3DG.m_config);
	if (errorCode != BIRD_ERROR_SUCCESS)
		return errorHandler(errorCode);

	//
	// GET SENSOR CONFIGURATION
	//
	// Having determined how many sensors can be supported we can dynamically
	// allocate storage for the information about each sensor.
	// This information is acquired through a call to GetSensorConfiguration()
	// This call will fill a fixed size structure containing amongst other things
	// a status which indicates whether a physical sensor is attached to this
	// sensor port or not.
	//

	BOOL buffer = true;			// set metric reporting = true    // true for mm and fasle for inch
	//BOOL *pBuffer = &buffer;
	errorCode = SetSystemParameter(METRIC, &buffer, sizeof(buffer));
	if (errorCode != BIRD_ERROR_SUCCESS)
	{
		return errorHandler(errorCode);
	}

	printf("\n set measurement rate");
	double rate = 150.0;
	errorCode = SetSystemParameter(MEASUREMENT_RATE, &rate, sizeof(rate));
	if (errorCode != BIRD_ERROR_SUCCESS)
	{
		return errorHandler(errorCode);
	}

	double temp = 0.0;
	errorCode = GetSystemParameter(MEASUREMENT_RATE, &temp, sizeof(temp));
	if (errorCode != BIRD_ERROR_SUCCESS)
	{
		return errorHandler(errorCode);
	}

	printf("\n measurement rate=%f", temp);

	pSensor = new CSensor[ATC3DG.m_config.numberSensors];
	for (int i = 0; i < ATC3DG.m_config.numberSensors; i++)
	{
		//printf("\nATC3DG.m_config.numberSensors=%d", ATC3DG.m_config.numberSensors);

		errorCode = GetSensorConfiguration(static_cast<USHORT>(i), &(pSensor + i)->m_config);
		if (errorCode != BIRD_ERROR_SUCCESS)
		{
			printf("\n get Sensor Config error");
			return errorHandler(errorCode);
		}
		//set data format of each sensor

#ifdef ANGELS_RECORD
		DATA_FORMAT_TYPE data_format_type = DOUBLE_POSITION_ANGLES_TIME_Q;
#else
		DATA_FORMAT_TYPE data_format_type = DOUBLE_POSITION_QUATERNION_TIME_Q;
#endif

		errorCode = SetSensorParameter(static_cast<USHORT>(i), DATA_FORMAT, (void*)&data_format_type, sizeof(DATA_FORMAT_TYPE));
		if (errorCode != BIRD_ERROR_SUCCESS) {
			printf("\n set Sensor param DATA_FORMAT error");
			return errorHandler(errorCode);
		}

		BOOL large_change = TRUE;

		errorCode = SetSensorParameter(static_cast<USHORT>(i), FILTER_LARGE_CHANGE, &large_change, sizeof(BOOL));
		if (errorCode != BIRD_ERROR_SUCCESS) {
			printf("\n set Sensor param FILTER_LARGE_CHANGE error");
			return errorHandler(errorCode);
		}

		DOUBLE_ANGLES_RECORD Angles = { 180.0,0.0,0.0 };
		errorCode = SetSensorParameter(static_cast<USHORT>(i), ANGLE_ALIGN, (void*)&Angles, sizeof(DOUBLE_ANGLES_RECORD));
		if (errorCode != BIRD_ERROR_SUCCESS) {
			printf("\n set Sensor param ANGLE_ALIGN error");
			return errorHandler(errorCode);
		}

		//DOUBLE_POSITION_RECORD Offset = {48.26,0.0,0.0};
		//errorCode = SetSensorParameter(static_cast<USHORT>(i),SENSOR_OFFSET,(void *)&Offset, sizeof(DOUBLE_POSITION_RECORD));
		//if (errorCode != BIRD_ERROR_SUCCESS){
		//	printf ("\n set Sensor param SENSOR_OFFSET error");
		//    return errorHandler(errorCode);
		//}
	}

	//
	// GET TRANSMITTER CONFIGURATION
	//
	// The call to GetTransmitterConfiguration() performs a similar task to the
	// GetSensorConfiguration() call. It also returns a status in the filled
	// structure which indicates whether a transmitter is attached to this
	// port or not. In a single transmitter system it is only necessary to
	// find where that transmitter is in order to turn it on and use it.
	//
	printf("\n get Transmitter Config");
	pXmtr = new CXmtr[ATC3DG.m_config.numberTransmitters];
	for (int i = 0; i < ATC3DG.m_config.numberTransmitters; i++)
	{
		errorCode = GetTransmitterConfiguration(static_cast<USHORT>(i), &(pXmtr + i)->m_config);
		if (errorCode != BIRD_ERROR_SUCCESS)
			return errorHandler(errorCode);
	}
	//
	// Search for the first attached transmitter and turn it on
	//
	printf("\n Turn ON first Transmitter");
	for (short id = 0; id < ATC3DG.m_config.numberTransmitters; id++)
	{
		if ((pXmtr + id)->m_config.attached)
		{
			// Transmitter selection is a system function.
			// Using the SELECT_TRANSMITTER parameter we send the id of the
			// transmitter that we want to run with the SetSystemParameter() call
			errorCode = SetSystemParameter(SELECT_TRANSMITTER, &id, sizeof(id));
			if (errorCode != BIRD_ERROR_SUCCESS) {
				return errorHandler(errorCode);
				break;
			}
		}
	}
	pRecord = &record;
	return true;
}


//
//	ERROR HANDLER
//	=============
//
// This is a simplified error handler.
// This error handler takes the error code and passes it to the GetErrorText()
// procedure along with a buffer to place an error message string.
// This error message string can then be output to a user display device
// like the console
// Specific error codes should be parsed depending on the application.
//
bool errorHandler(int error)
{
	char			buffer[1024];
	char* pBuffer = &buffer[0];
	int				numberBytes;

	int id = -1;
	error = SetSystemParameter(SELECT_TRANSMITTER, &id, sizeof(id));
	while (error != BIRD_ERROR_SUCCESS)
	{
		error = GetErrorText(error, pBuffer, sizeof(buffer), SIMPLE_MESSAGE);
		numberBytes = static_cast<int>(strlen(buffer));
		buffer[numberBytes] = '\n';		// append a newline to buffer
		printf("\n %s", buffer);
	}
	BirdInitSuccess = false;
	//exit(0);
	return false;
}





PLCUpdater::PLCUpdater()
    :m_Running(false)
{


	std::cout << "PLCUpdater" << std::endl;
    // Initialize ADS
    AdsPortOpen();
    if (AdsGetLocalAddress(&m_Addr) != ADSERR_NOERR) {
        std::cerr << "Error: AdsGetLocalAddress failed" << std::endl;
    }

    // Connect to remote ADS device
    m_Addr.netId.b[0] = 172;
    m_Addr.netId.b[1] = 16;
    m_Addr.netId.b[2] = 6;
    m_Addr.netId.b[3] = 66;
    m_Addr.netId.b[4] = 1;
    m_Addr.netId.b[5] = 1;
    m_Addr.port = 301;

    // Set variable names
    m_VariableNames.leftX = "Task 2.Outputs.leftX";
    m_VariableNames.leftY = "Task 2.Outputs.leftY";
    m_VariableNames.leftZ = "Task 2.Outputs.leftZ";
    m_VariableNames.leftQ0 = "Task 2.Outputs.leftQ0";
    m_VariableNames.leftQ1 = "Task 2.Outputs.leftQ1";
    m_VariableNames.leftQ2 = "Task 2.Outputs.leftQ2";
    m_VariableNames.leftQ3 = "Task 2.Outputs.leftQ3";
    m_VariableNames.rightX = "Task 2.Outputs.rightX";
    m_VariableNames.rightY = "Task 2.Outputs.rightY";
    m_VariableNames.rightZ = "Task 2.Outputs.rightZ";
    m_VariableNames.rightQ0 = "Task 2.Outputs.rightQ0";
    m_VariableNames.rightQ1 = "Task 2.Outputs.rightQ1";
    m_VariableNames.rightQ2 = "Task 2.Outputs.rightQ2";
    m_VariableNames.rightQ3 = "Task 2.Outputs.rightQ3";
    m_VariableNames.H1_GRP = "Task 2.Outputs.H1_GRP";
    m_VariableNames.H2_GRP = "Task 2.Outputs.H2_GRP";
    m_VariableNames.lefttIsVirtualBoundary = "Task 2.Outputs.lefttIsVirtualBoundary";
    m_VariableNames.leftQuality = "Task 2.Outputs.leftQuality";
    m_VariableNames.START_BIT = "Task 2.Outputs.START_BIT";
    m_VariableNames.F1_FP = "Task 2.Outputs.F1_FP";
    m_VariableNames.F2_FP = "Task 2.Outputs.F2_FP";
    m_VariableNames.rightQuality = "Task 2.Outputs.rightQuality";
    m_VariableNames.H1_GRP_CHECK = "Task 2.Outputs.H1_GRP_CHECK";
    m_VariableNames.rightIsVirtualBoundary = "Task 2.Outputs.rightIsVirtualBoundary";
    m_VariableNames.H2_GRP_CHECK = "Task 2.Outputs.H2_GRP_CHECK";
    m_VariableNames.STOP_BIT = "Task 2.Outputs.STOP_BIT";




    // Get handles for PLC variables
    if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle1), &m_Handle1, m_VariableNames.leftX.size() + 1, (void*)m_VariableNames.leftX.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var1 failed" << std::endl;
    }
    if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle2), &m_Handle2, m_VariableNames.leftY.size() + 1, (void*)m_VariableNames.leftY.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var2 failed" << std::endl;
    }

    if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle3), &m_Handle3, m_VariableNames.leftZ.size() + 1, (void*)m_VariableNames.leftZ.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var1 failed" << std::endl;
    }

    if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle4), &m_Handle4, m_VariableNames.leftQ0.size() + 1, (void*)m_VariableNames.leftQ0.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var2 failed" << std::endl;
    }

    if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle5), &m_Handle5, m_VariableNames.leftQ1.size() + 1, (void*)m_VariableNames.leftQ1.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var1 failed" << std::endl;
    }
    if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle6), &m_Handle6, m_VariableNames.leftQ2.size() + 1, (void*)m_VariableNames.leftQ2.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var2 failed" << std::endl;
    }

    if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle7), &m_Handle7, m_VariableNames.leftQ3.size() + 1, (void*)m_VariableNames.leftQ3.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var1 failed" << std::endl;
    }

    if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle8), &m_Handle8, m_VariableNames.rightX.size() + 1, (void*)m_VariableNames.rightX.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var2 failed" << std::endl;
    }


    if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle9), &m_Handle9, m_VariableNames.rightY.size() + 1, (void*)m_VariableNames.rightY.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var1 failed" << std::endl;
    }
    if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle10), &m_Handle10, m_VariableNames.rightZ.size() + 1, (void*)m_VariableNames.rightZ.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var2 failed" << std::endl;
    }

    if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle11), &m_Handle11, m_VariableNames.rightQ0.size() + 1, (void*)m_VariableNames.rightQ0.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var1 failed" << std::endl;
    }

    if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle12), &m_Handle12, m_VariableNames.rightQ1.size() + 1, (void*)m_VariableNames.rightQ1.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var2 failed" << std::endl;
    }

    if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle13), &m_Handle13, m_VariableNames.rightQ2.size() + 1, (void*)m_VariableNames.rightQ2.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var1 failed" << std::endl;
    }

    if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle14), &m_Handle14, m_VariableNames.rightQ3.size() + 1, (void*)m_VariableNames.rightQ3.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var1 failed" << std::endl;
    }

	if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle22), &m_Handle22, m_VariableNames.rightQuality.size() + 1, (void*)m_VariableNames.rightQuality.c_str()) != ADSERR_NOERR) {
		std::cerr << "Error: AdsSyncReadWriteReq for Var2 failed" << std::endl;
	}

	if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle18), &m_Handle18, m_VariableNames.leftQuality.size() + 1, (void*)m_VariableNames.leftQuality.c_str()) != ADSERR_NOERR) {
		std::cerr << "Error: AdsSyncReadWriteReq for Var2 failed" << std::endl;
	}


   /* if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle15), &m_Handle15, m_VariableNames.H1_GRP.size() + 1, (void*)m_VariableNames.H1_GRP.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var2 failed" << std::endl;
    }

    if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle16), &m_Handle16, m_VariableNames.H2_GRP.size() + 1, (void*)m_VariableNames.H2_GRP.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var1 failed" << std::endl;
    }

   if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle17), &m_Handle17, m_VariableNames.lefttIsVirtualBoundary.size() + 1, (void*)m_VariableNames.lefttIsVirtualBoundary.c_str()) != ADSERR_NOERR) {
        std::cerr << "Error: AdsSyncReadWriteReq for Var2 failed" << std::endl;
    }

  

   if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle19), &m_Handle19, m_VariableNames.START_BIT.size() + 1, (void*)m_VariableNames.START_BIT.c_str()) != ADSERR_NOERR) {
       std::cerr << "Error: AdsSyncReadWriteReq for Var1 failed" << std::endl;
   }
   if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle20), &m_Handle20, m_VariableNames.F1_FP.size() + 1, (void*)m_VariableNames.F1_FP.c_str()) != ADSERR_NOERR) {
       std::cerr << "Error: AdsSyncReadWriteReq for Var2 failed" << std::endl;
   }

   if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle21), &m_Handle21, m_VariableNames.F2_FP.size() + 1, (void*)m_VariableNames.F2_FP.c_str()) != ADSERR_NOERR) {
       std::cerr << "Error: AdsSyncReadWriteReq for Var1 failed" << std::endl;
   }



   if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle23), &m_Handle23, m_VariableNames.H1_GRP_CHECK.size() + 1, (void*)m_VariableNames.H1_GRP_CHECK.c_str()) != ADSERR_NOERR) {
       std::cerr << "Error: AdsSyncReadWriteReq for Var2 failed" << std::endl;
   }

   if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle24), &m_Handle24, m_VariableNames.rightIsVirtualBoundary.size() + 1, (void*)m_VariableNames.rightIsVirtualBoundary.c_str()) != ADSERR_NOERR) {
       std::cerr << "Error: AdsSyncReadWriteReq for Var2 failed" << std::endl;
   }

   if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle25), &m_Handle25, m_VariableNames.H2_GRP_CHECK.size() + 1, (void*)m_VariableNames.H2_GRP_CHECK.c_str()) != ADSERR_NOERR) {
       std::cerr << "Error: AdsSyncReadWriteReq for Var1 failed" << std::endl;
   }

   if (AdsSyncReadWriteReq(&m_Addr, ADSIGRP_SYM_HNDBYNAME, 0x0, sizeof(m_Handle26), &m_Handle26, m_VariableNames.STOP_BIT.size() + 1, (void*)m_VariableNames.STOP_BIT.c_str()) != ADSERR_NOERR) {
       std::cerr << "Error: AdsSyncReadWriteReq for Var2 failed" << std::endl;
   }*/




}


void PLCUpdater::UpdateValues()
{
	std::cout << "UpdateValues" << std::endl;
	
	while (1)
	{

		int sensorID = 0, errorCode;

		for (sensorID; sensorID < 2; sensorID++)
		{

			//std::cout << "UpdateValues1" << std::endl;
			//printf("\n sensorID: %d\n\tx:%08.2f\n\ty:%08.2f\n\tz:%08.2f\n\tq[0]:%08.2f\n\tq[1]:%08.2f\n\tq[2]:%08.2f\n\tq[3]:%08.2f\n\tquality:%05u\n\ttime:%f", sData.SensorID, sData.rec.x, sData.rec.y, sData.rec.z, sData.rec.q[0], sData.rec.q[1], sData.rec.q[2], sData.rec.q[3], sData.rec.quality, sData.rec.time);

			float M2mm = 1000;

			// sensor attached so get record
			errorCode = GetAsynchronousRecord(sensorID, pRecord, sizeof(record));
			if (errorCode != BIRD_ERROR_SUCCESS)
			{
				errorHandler(errorCode);
			}



			// get the status of the last data record
			// only report the data if everything is okay
			DEVICE_STATUS _status = GetSensorStatus(sensorID);

			if (_status == VALID_STATUS || _status == OUT_OF_MOTIONBOX)
			{

				//std::cout << "UpdateValues2" << std::endl;
				if (sensorID == 0)
				{

					std::cout << "sensorID 0" << std::endl;

					tele.leftX = static_cast<float>(record.x / M2mm);
					if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle1, sizeof(tele.leftX), &tele.leftX) != ADSERR_NOERR) {
						std::cerr << "Error: AdsSyncWriteReq for leftX failed" << std::endl;
					}


					tele.leftY = static_cast<float>(record.y / M2mm);
					if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle2, sizeof(tele.leftY), &tele.leftY) != ADSERR_NOERR) {
						std::cerr << "Error: AdsSyncWriteReq for leftY failed" << std::endl;
					}

					tele.leftZ = static_cast<float>(record.z / M2mm);
					if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle3, sizeof(tele.leftZ), &tele.leftZ) != ADSERR_NOERR) {
						std::cerr << "Error: AdsSyncWriteReq for leftX failed" << std::endl;
					}

					tele.leftQ0 = static_cast<float>(record.q[0]);
					if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle4, sizeof(tele.leftQ0), &tele.leftQ0) != ADSERR_NOERR) {
						std::cerr << "Error: AdsSyncWriteReq for leftY failed" << std::endl;
					}

					tele.leftQ1 = static_cast<float>(record.q[1]);
					if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle5, sizeof(tele.leftQ1), &tele.leftQ1) != ADSERR_NOERR) {
						std::cerr << "Error: AdsSyncWriteReq for leftX failed" << std::endl;
					}

					tele.leftQ2 = static_cast<float>(record.q[2]);
					if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle6, sizeof(tele.leftQ2), &tele.leftQ2) != ADSERR_NOERR) {
						std::cerr << "Error: AdsSyncWriteReq for leftY failed" << std::endl;
					}
					tele.leftQ3 = static_cast<float>(record.q[3]);
					if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle7, sizeof(tele.leftQ3), &tele.leftQ3) != ADSERR_NOERR) {
						std::cerr << "Error: AdsSyncWriteReq for leftX failed" << std::endl;
					}

					tele.leftQuality = static_cast<int>(record.quality / 250);
					if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle18, sizeof(tele.leftQuality), &tele.leftQuality) != ADSERR_NOERR) {
						std::cerr << "Error: AdsSyncWriteReq for leftX failed" << std::endl;
					}


				}

				else if (sensorID == 1)
				{


					std::cout << "sensorID 1" << std::endl;

					tele.rightX = static_cast<float>(record.x / M2mm);
					if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle8, sizeof(tele.rightX), &tele.rightX) != ADSERR_NOERR) {
						std::cerr << "Error: AdsSyncWriteReq for leftY failed" << std::endl;
					}
					tele.rightY = static_cast<float>(record.y / M2mm);
					if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle9, sizeof(tele.rightY), &tele.rightY) != ADSERR_NOERR) {
						std::cerr << "Error: AdsSyncWriteReq for leftX failed" << std::endl;
					}
					tele.rightZ = static_cast<float>(record.z / M2mm);
					if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle10, sizeof(tele.rightZ), &tele.rightZ) != ADSERR_NOERR) {
						std::cerr << "Error: AdsSyncWriteReq for leftY failed" << std::endl;
					}
					tele.rightQ0 = static_cast<float>(record.q[0]);
					if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle11, sizeof(tele.rightQ0), &tele.rightQ0) != ADSERR_NOERR) {
						std::cerr << "Error: AdsSyncWriteReq for leftX failed" << std::endl;
					}
					tele.rightQ1 = static_cast<float>(record.q[1]);
					if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle12, sizeof(tele.rightQ1), &tele.rightQ1) != ADSERR_NOERR) {
						std::cerr << "Error: AdsSyncWriteReq for leftY failed" << std::endl;
					}

					tele.rightQ2 = static_cast<float>(record.q[2]);
					if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle13, sizeof(tele.rightQ2), &tele.rightQ2) != ADSERR_NOERR) {
						std::cerr << "Error: AdsSyncWriteReq for leftY failed" << std::endl;
					}
					tele.rightQ3 = static_cast<float>(record.q[3]);
					if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle14, sizeof(tele.rightQ3), &tele.rightQ3) != ADSERR_NOERR) {
						std::cerr << "Error: AdsSyncWriteReq for leftX failed" << std::endl;
					}

					tele.rightQuality = (int)record.quality / 250;
					if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle22, sizeof(tele.rightQuality), &tele.rightQuality) != ADSERR_NOERR) {
						std::cerr << "Error: AdsSyncWriteReq for leftY failed" << std::endl;
					}

					/* tele.H1_GRP = 13.0f;
					 tele.H2_GRP = 14.0f;
					 tele.lefttIsVirtualBoundary = 15.0f;
					 tele.leftQuality = 16.0f;
					 tele.START_BIT = 17.0f;
					 tele.F1_FP = 18.0f;
					 tele.F2_FP = 19.0f;
					 tele.rightQuality = 20.f;
					 tele.H1_GRP_CHECK = 21.0f;
					 tele.rightIsVirtualBoundary = 22.0f;
					 tele.H2_GRP_CHECK = 23.0f;
					 tele.STOP_BIT = 24.0f;*/
				}




				//comdata1.SensorID = sensorID;
				//comdata1.start = 0x5A;
				//comdata1.x = (float)record.x / M2mm;
				//comdata1.y = (float)record.y / M2mm;
				//comdata1.z = (float)record.z / M2mm;
				//comdata1.q[0] = (float)record.q[0];
				//comdata1.q[1] = (float)record.q[1];
				//comdata1.q[2] = (float)record.q[2];
				//comdata1.q[3] = (float)record.q[3];
				//comdata1.quality = (int)record.quality / 250;





			//	printf("\nStart=%X\n\tsensorID: %d\n\tx:%08.2f\n\ty:%08.2f\n\tz:%08.2f\n\tq[0]:%08.2f\n\tq[1]:%08.2f\n\tq[2]:%08.2f\n\tq[3]:%08.2f\n\tquality:%d\n",
				//	comdata1.start, comdata1.SensorID, comdata1.x, comdata1.y, comdata1.z, comdata1.q[0], comdata1.q[1], comdata1.q[2], comdata1.q[3], comdata1.quality);




			}



		}


	}

	
	/*
	if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle15, sizeof(tele.H1_GRP), &tele.H1_GRP) != ADSERR_NOERR) {
		std::cerr << "Error: AdsSyncWriteReq for leftY failed" << std::endl;
	}

	if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle16, sizeof(tele.H2_GRP), &tele.H2_GRP) != ADSERR_NOERR) {
		std::cerr << "Error: AdsSyncWriteReq for leftX failed" << std::endl;
	}

	if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle17, sizeof(tele.lefttIsVirtualBoundary), &tele.lefttIsVirtualBoundary) != ADSERR_NOERR) {
		std::cerr << "Error: AdsSyncWriteReq for leftY failed" << std::endl;
	}

	if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle19, sizeof(tele.START_BIT), &tele.START_BIT) != ADSERR_NOERR) {
		std::cerr << "Error: AdsSyncWriteReq for leftY failed" << std::endl;
	}


	if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle20, sizeof(tele.F1_FP), &tele.F1_FP) != ADSERR_NOERR) {
		std::cerr << "Error: AdsSyncWriteReq for leftY failed" << std::endl;
	}

	if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle21, sizeof(tele.F2_FP), &tele.F2_FP) != ADSERR_NOERR) {
		std::cerr << "Error: AdsSyncWriteReq for leftX failed" << std::endl;
	}


	if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle23, sizeof(tele.H1_GRP_CHECK), &tele.H1_GRP_CHECK) != ADSERR_NOERR) {
		std::cerr << "Error: AdsSyncWriteReq for leftX failed" << std::endl;
	}

	if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle24, sizeof(tele.rightIsVirtualBoundary), &tele.rightIsVirtualBoundary) != ADSERR_NOERR) {
		std::cerr << "Error: AdsSyncWriteReq for leftY failed" << std::endl;
	}

	if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle25, sizeof(tele.H2_GRP_CHECK), &tele.H2_GRP_CHECK) != ADSERR_NOERR) {
		std::cerr << "Error: AdsSyncWriteReq for leftX failed" << std::endl;
	}

	if (AdsSyncWriteReq(&m_Addr, ADSIGRP_SYM_VALBYHND, m_Handle26, sizeof(tele.STOP_BIT), &tele.STOP_BIT) != ADSERR_NOERR) {
		std::cerr << "Error: AdsSyncWriteReq for leftY failed" << std::endl;
	}*/



}



PLCUpdater::~PLCUpdater()
{

	// Stop();
	// AdsPortClose();
}

void PLCUpdater::Start()
{
	while (1)
	{
		//m_Running = true;
		UpdateValues();

	}
	//   m_UpdateThread = std::thread(&PLCUpdater::UpdateLoop, this);
}

void PLCUpdater::Stop()
{
	/*   if (m_Running) {
		   m_Running = false;
		   if (m_UpdateThread.joinable()) {
			   m_UpdateThread.join();
		   }
	   }*/
}

void PLCUpdater::UpdateLoop()
{
	//   while(1) 
	   //{
	//      
	//        // Add a delay to avoid tight loop
	//   }
}


int main(int argc, char* argv[])
{

	int sid, errorCode;

	PLCUpdater updater;
	while (1)
	{
		if (BirdInitSuccess == false) {
			if (InitBirdSystem()) {
				//#ifdef ENABLE_LOGGING
				//				time(&rawtime);
				//				t = localtime(&rawtime);
				//				sprintf(filename, "./NDI_Data_%04d%02d%02d%02d%02d%02d.log", t->tm_year + 1900, t->tm_mon + 1, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);
				//				fp = open(filename, O_CREAT | O_WRONLY);
				//#endif
				BirdInitSuccess = true;
				printf("Bird System Init Success\n");
			}
			else {
				printf("\n Bird System Init Fail");
				BirdInitSuccess = false;
				//#ifdef ENABLE_LOGGING
				//				close(fp);
				//#endif

			}
		}


		else
		{


			
			updater.Start();

			// Allow the program to run indefinitely
		


		}



			}


	//std::cin.get(); // Wait for user input to stop the program
	//updater.Stop(); // Stop the updater
}
	



